﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    [Header("Editable player setting")]
    public float speed = 2.5f;
    public float force = 4.0f;
    public Text text; 
    

    private Rigidbody2D _rigidbody2D;
    private SpriteRenderer _spriteRenderer;
    private bool isEnd = false;

    private void Awake() {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    private void Update() {
        if (!isEnd) {
            if (transform.position.y < -10) {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            if (Input.GetKey(KeyCode.A)) {
                transform.Translate(Vector2.left * (Time.deltaTime * speed));
                _spriteRenderer.flipX = true;
            }
            if (Input.GetKey(KeyCode.D)) {
                transform.Translate(Vector2.right * (Time.deltaTime * speed));
                _spriteRenderer.flipX = false;
            }
            if (Input.GetKeyDown(KeyCode.Space)) {
                _rigidbody2D.AddForce(Vector2.up * force, ForceMode2D.Impulse);
            }
        } else {
            if (Input.GetKey(KeyCode.R)) {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        Debug.Log("Enter to collisiion");
        if (other.gameObject.CompareTag("Respawn")) {
            if (!isEnd) {
                isEnd = true;
                if (text != null) {
                    text.enabled = true;
                }
            }
        }
    }
    
}
