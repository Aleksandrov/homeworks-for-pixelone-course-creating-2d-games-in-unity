﻿using UnityEngine;

public class Hw2P1 : MonoBehaviour {

    public int sideOfSquare = 2;
    public int sideAofRectangle = 3;
    public int sideBOfRectangle = 4;
    public float circleRadius = 3.5f;
    
    // Start is called before the first frame update
    void Start() {
        int squareArea = sideOfSquare * sideOfSquare;
        int rectangleArea = sideAofRectangle * sideBOfRectangle;
        float circleArea = Mathf.PI * circleRadius * circleRadius;
        Debug.Log($"Площадь квадрата со стороной {sideOfSquare} равна {squareArea}");
        Debug.Log($"Площадь прямоугольника со cторонами A = {sideAofRectangle} и B = {sideBOfRectangle} равна {rectangleArea}");
        Debug.Log($"Площадь окружности с радиусом R = {circleRadius} равна {circleArea}");
    }
    
}
