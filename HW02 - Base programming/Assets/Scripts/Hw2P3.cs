﻿using UnityEngine;

public class Hw2P3 : MonoBehaviour {
    // Start is called before the first frame update
    void Start() {
        long paperThickness = 1;
        long distanceFromEarthToMoon = 300000L * 1000L * 100L * 10L;
        int count = 0;
        while (paperThickness < distanceFromEarthToMoon) {
            paperThickness *= 2;
            count++;
        }
        Debug.Log($"{count} раз необходимо сложить бесконечный лист бумаги что бы достичь Луны");
    }
    
}
