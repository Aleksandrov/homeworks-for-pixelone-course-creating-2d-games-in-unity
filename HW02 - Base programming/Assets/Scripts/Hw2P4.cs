﻿using UnityEngine;
using Random = UnityEngine.Random;

public class Hw2P4 : MonoBehaviour {

    public SpriteRenderer[] spriteRenderers;
    
    // Start is called before the first frame update
    void Start() {
        foreach (var spriteRenderer in spriteRenderers) {
            spriteRenderer.color = new Color(Random.value, Random.value, Random.value, 1f);
        }
    }
    
}
