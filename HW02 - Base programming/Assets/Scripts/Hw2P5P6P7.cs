﻿using UnityEngine;

public class Hw2P5P6P7 : MonoBehaviour {
    // Start is called before the first frame update
    void Start() {
        Debug.Log(compare(3, 4));
        printMessage();
        Debug.Log(circleArea(3.5f));
    }

    public bool compare(int x, int y) {
        return x > y;
    }

    public void printMessage() {
        Debug.Log("Hello World!");
    }

    public float circleArea(float radius) {
        return Mathf.PI * (radius * radius);
    }
    
}
