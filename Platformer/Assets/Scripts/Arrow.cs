﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(
    typeof(Rigidbody2D),
    typeof(SpriteRenderer),
    typeof(TriggerDamage)
    )
]
public class Arrow : MonoBehaviour, IObjectDestroyer {

    [SerializeField] private float force;
    [SerializeField] private float lifeTime;
    
    private SpriteRenderer m_SpriteRenderer;
    private Rigidbody2D m_Rigidbody2D;
    private TriggerDamage m_TriggerDamage;
    private PlayerController player;
    
    public float Force {
        get => force;
        set => force = value;
    }

    private void Awake() {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
        m_TriggerDamage = GetComponent<TriggerDamage>();
    }
    
    public void Destroy(GameObject gameObj) {
        player.ReturnArrowToPool(this);
    }

    public void SetImpulse(Vector2 direction, float force, int bonusDamage, PlayerController player) {
        this.player = player;
        m_TriggerDamage.Init(this);
        m_TriggerDamage.Parent = player.gameObject;
        m_TriggerDamage.Damage += bonusDamage;
        m_Rigidbody2D.AddForce(direction * force, ForceMode2D.Impulse);
        if (force < 0) {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        StartCoroutine(StartLife());
    }

    private IEnumerator StartLife() {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }
}
