﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffEmitter : MonoBehaviour {

    [SerializeField] private Buff buff;

    private void OnTriggerEnter2D(Collider2D other) {
        if (GameManager.Instance.buffReceiverContainer.ContainsKey(other.gameObject)) {
            var receiver = GameManager.Instance.buffReceiverContainer[other.gameObject];
            receiver.AddBuff(buff);
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (GameManager.Instance.buffReceiverContainer.ContainsKey(other.gameObject)) {
            var receiver = GameManager.Instance.buffReceiverContainer[other.gameObject];
            receiver.RemoveBuff(buff);
        }
    }
}

[System.Serializable]
public class Buff {
    public BuffType type;
    public float additiveBonus;
    public float multipleBonus;
}

public enum BuffType : byte {
    Damage, Force, Armor
}
