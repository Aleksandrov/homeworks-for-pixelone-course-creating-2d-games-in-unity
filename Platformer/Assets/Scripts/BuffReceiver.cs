﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BuffReceiver : MonoBehaviour {

    private List<Buff> buffs;
    public Action OnBuffChanged;

    private void Start() {
        GameManager.Instance.buffReceiverContainer.Add(gameObject, this);
        buffs = new List<Buff>();
    }

    public List<Buff> Buffs => buffs;

    public void AddBuff(Buff buff) {
        if (!buffs.Contains(buff)) {
            buffs.Add(buff);
        }
        if (OnBuffChanged != null) {
            OnBuffChanged();
        }
    }

    public void RemoveBuff(Buff buff) {
        if (buffs.Contains(buff)) {
            buffs.Remove(buff);
        }
        if (OnBuffChanged != null) {
            OnBuffChanged();
        }
    }
}
