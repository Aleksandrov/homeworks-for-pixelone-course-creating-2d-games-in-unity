﻿using UnityEngine;

public class Coin : MonoBehaviour {

    [SerializeField] private Animator m_Animator;
    private static readonly int StartDestroy1 = Animator.StringToHash("StartDestroy");

    private void Start() {
        GameManager.Instance.coinContainer.Add(gameObject, this);
    }

    public void StartDestroy() {
        m_Animator.SetTrigger(StartDestroy1);
    }

    public void EndDestroy() {
        GameManager.Instance.inventory.CoinsCount++;
        Destroy(gameObject);
    }

}
