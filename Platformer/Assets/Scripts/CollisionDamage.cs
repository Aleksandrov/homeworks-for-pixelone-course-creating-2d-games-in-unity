﻿using System;
using UnityEngine;

public class CollisionDamage : MonoBehaviour {

    [SerializeField] private int damage = 10;
    [SerializeField] private Animator animator;

    private Health m_Health;
    private static readonly int Direction = Animator.StringToHash("Direction");

    public float direction { get; private set; }

    private void OnCollisionStay2D(Collision2D other) {
        if (GameManager.Instance.healthContainer.ContainsKey(other.gameObject)) {
            m_Health = GameManager.Instance.healthContainer[other.gameObject];
            direction = (other.transform.position - transform.position).x;
            animator.SetFloat(Direction, Mathf.Abs(direction));
        }
    }

    private void OnCollisionExit2D(Collision2D other) {
        Reset();
    }

    private void OnTriggerStay2D(Collider2D other) {
        if (GameManager.Instance.healthContainer.ContainsKey(other.gameObject)) {
            m_Health = GameManager.Instance.healthContainer[other.gameObject];
            direction = (other.transform.position - transform.position).x;
            animator.SetFloat(Direction, Mathf.Abs(direction));
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        Reset();
    }

    public void SetDamage() {
        if (m_Health != null) {
            m_Health.TakeHit(damage);
        }
        Reset();
    }

    private void Reset() {
        direction = 0f;
        animator.SetFloat(Direction, 0f);
        m_Health = null;
    }
    
}
