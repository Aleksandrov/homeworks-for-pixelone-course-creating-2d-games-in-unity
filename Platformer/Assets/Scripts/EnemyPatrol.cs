﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyPatrol : MonoBehaviour {

    [SerializeField] private GameObject leftBorder;
    [SerializeField] private  GameObject rightBorder;
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private SpriteRenderer spriteRender;
    [SerializeField] private CollisionDamage collisionDamage;
    [SerializeField] private GroundDetection groundDetection;
    [SerializeField] private Animator animator;
    
    private bool m_IsRightDirection;
    
    private void Update() {
        if (groundDetection.isGrounded) {
            if (transform.position.x > rightBorder.transform.position.x || collisionDamage.direction < 0) {
                m_IsRightDirection = false;
            } else if (transform.position.x < leftBorder.transform.position.x || collisionDamage.direction > 0) {
                m_IsRightDirection = true;
            }
            _rigidbody.velocity = m_IsRightDirection ? Vector2.right : Vector2.left;
            _rigidbody.velocity *= speed;
        }
        if (_rigidbody.velocity.x > 0) {
            spriteRender.flipX = true;
        }
        if (_rigidbody.velocity.x < 0) {
            spriteRender.flipX = false;
        }
    }
}
