﻿using UnityEngine;

public class FollowCamera : MonoBehaviour {

    [Header("Set in Inspector")]
    public GameObject POI;
    public float easing = 0.05f;
    public Vector2 minXY = Vector2.zero;
    
    private float m_CamZ;

    private void Awake() {
        m_CamZ = transform.position.z;
    }

    private void FixedUpdate() {
        var destination = POI == null ? Vector3.zero : POI.transform.position;
        destination.x = Mathf.Max(minXY.x, destination.x);
        destination.y = Mathf.Max(minXY.y, destination.y);
        destination = Vector3.Lerp(transform.position, destination, easing);
        destination.z = m_CamZ;
        transform.position = destination;
    }
    
}
