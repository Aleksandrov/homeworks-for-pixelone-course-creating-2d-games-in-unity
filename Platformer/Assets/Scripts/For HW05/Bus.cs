﻿using UnityEngine;

public class Bus: Vehicle {
    
    public Bus() {
        vehicleName = "Bus";
    }
    
    public override void Beep() {
        base.Beep();
        Debug.Log($"{vehicleName}: beeeep");
    }
    
}
