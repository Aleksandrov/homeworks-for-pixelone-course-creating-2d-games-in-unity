﻿using UnityEngine;

public class Car: Vehicle {
    
    public Car() {
        vehicleName = "Car";
    }
    
    public override void Beep() {
        base.Beep();
        Debug.Log($"{vehicleName}: beep");
    }
}
