﻿using UnityEngine;

public class Cat {

    public string name { get; }
    public int age { get; }
    public int weight { get; set; }
    public int height { get; }
    public int length { get; }

    public Cat() {
    }

    public Cat(string name, int age, int weight, int height, int length) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.height = height;
        this.length = length;
    }

    public void meov() {
        Debug.Log(this);
    }

    public override string ToString() {
        return $"Cat: \"name\": \"{name}\", \"age\": {age}, \"weight\": {weight}, \"height\": {height}, \"length\": {length}'";
    }
}
