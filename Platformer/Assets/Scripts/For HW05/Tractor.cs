﻿using UnityEngine;

public class Tractor: Vehicle {

    public Tractor() {
        vehicleName = "Tractor";
    }
    
    public override void Beep() {
        base.Beep();
        Debug.Log($"{vehicleName}: trrr");
    }
    
}
