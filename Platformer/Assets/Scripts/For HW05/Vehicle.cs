﻿using UnityEngine;

abstract public class Vehicle {

    public string vehicleName { get; set; }

    public virtual void Beep() {
        Debug.Log("Vehicle");
    }

}
