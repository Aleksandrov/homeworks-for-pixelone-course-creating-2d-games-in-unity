﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    #region Singleton
    public static GameManager Instance { get; private set; }
    #endregion

    public Dictionary<GameObject, Health> healthContainer;
    public Dictionary<GameObject, Coin> coinContainer;
    public Dictionary<GameObject, BuffReceiver> buffReceiverContainer;
    public Dictionary<GameObject, ItemComponent> itemsContainer;
    [HideInInspector] public PlayerInventory inventory;
    public ItemBase ItemBase;

    private void Awake() {
        Instance = this;
        healthContainer = new Dictionary<GameObject, Health>();
        coinContainer = new Dictionary<GameObject, Coin>();
        buffReceiverContainer = new Dictionary<GameObject, BuffReceiver>();
        itemsContainer = new Dictionary<GameObject, ItemComponent>();
    }

   
    //Вариант создания пула объектов
    // private void Start() {
    //     var healthObjects = FindObjectsOfType<Health>();
    //     foreach (var health in healthObjects) {
    //         healthContainer.Add(health.gameObject, health);
    //     }
    // }
}
