﻿using UnityEngine;

public class GroundDetection : MonoBehaviour {

    public bool isGrounded { get; private set; }

    private void OnCollisionStay2D(Collision2D other) {
        
        if (other.gameObject.CompareTag("Platform") || other.gameObject.CompareTag("Enemy")) {
            isGrounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D other) {
        if (other.gameObject.CompareTag("Platform") || other.gameObject.CompareTag("Enemy")) {
            isGrounded = false;
        } 
    }
    
}
