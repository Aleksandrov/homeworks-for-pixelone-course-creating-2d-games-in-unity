﻿using UnityEngine;

public class Health : MonoBehaviour {

    [SerializeField] private int health;
    [SerializeField] private Animator animator;
    private static readonly int TakeDamage = Animator.StringToHash("TakeDamage");

    public int currentHealth { get; private set; }

    private void Start() {
        currentHealth = health;
        GameManager.Instance.healthContainer.Add(gameObject, this);
    }

    public void TakeHit(int damage) {
        currentHealth -= damage;
        animator.SetTrigger(TakeDamage);
        if (currentHealth <= 0) {
            Destroy(gameObject);
        }
    }

    public void SetHealth(int bonusHealth) {
        currentHealth += bonusHealth;
        // if (currentHealth > health) {
        //     currentHealth = health;
        // }
    }

}
