﻿using UnityEngine;

public class HealthBonus : MonoBehaviour {

    [SerializeField] private int bonusHealth = 10;
    [SerializeField] private Animator animator;

    private Health health;
    private static readonly int Pickup = Animator.StringToHash("Pickup");

    private void OnTriggerEnter2D(Collider2D other) {
        if (GameManager.Instance.healthContainer.ContainsKey(other.gameObject)) {
            health = GameManager.Instance.healthContainer[other.gameObject];
            animator.SetTrigger(Pickup);
        }
    }

    public void PickupHealthBonus() {
        if (health != null) {
            health.SetHealth(bonusHealth);
            Destroy(gameObject);
        }
    } 
}
