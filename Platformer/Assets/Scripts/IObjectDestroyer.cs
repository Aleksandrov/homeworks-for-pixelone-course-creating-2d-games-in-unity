using UnityEngine;

public interface IObjectDestroyer {
    void Destroy(GameObject gameObj);
}