﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUiController : MonoBehaviour {

    [SerializeField] private Cell[] cells;
    [SerializeField] private int cellCount;
    [SerializeField] private Cell cellPrefab;
    [SerializeField] private Transform rootParent;

    // Start is called before the first frame update
    private void Init() {
        cells = new Cell[cellCount];
        for (var i = 0; i < cellCount; i++) {
            cells[i] =  Instantiate(cellPrefab, rootParent);
            cells[i].OnUpdateCell += UpdateInventory;
        }
        cellPrefab.gameObject.SetActive(false);
    }

    private void OnEnable() {
        if (cells == null || cells.Length <= 0) {
            Init();
        }
        UpdateInventory();
    }

    public void UpdateInventory() {
        var inventory = GameManager.Instance.inventory;
        foreach (var cell in cells) {
            cell.Init(null);
        }
        for (var i = 0; i < inventory.Items.Count; i++) {
            if (i < cells.Length) {
                cells[i].Init(inventory.Items[i]);
            }
        }
    }
}
