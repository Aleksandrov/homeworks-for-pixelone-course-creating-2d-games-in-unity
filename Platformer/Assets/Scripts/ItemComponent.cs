﻿using UnityEngine;

public class ItemComponent : MonoBehaviour, IObjectDestroyer {

    [SerializeField] private ItemType type;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Animator animator;

    private Item _item;

    public Item Item => _item;

    private void Start() {
        _item = GameManager.Instance.ItemBase.GetItemOfId((int) type);
        spriteRenderer.sprite = _item.Icon;
        GameManager.Instance.itemsContainer.Add(gameObject, this);
    }

    public void Destroy(GameObject gameObj) {
        animator.SetTrigger("StartDestroy");
    }

    public void EndDestroy() {
        MonoBehaviour.Destroy(gameObject);
    }
}

public enum ItemType {
    ForcePotion = 0, DamagePotion = 1, ArmorPotion = 2 
}