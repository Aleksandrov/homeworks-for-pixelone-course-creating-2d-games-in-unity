﻿using UnityEngine;

public class MushroomJumper : MonoBehaviour {

    [SerializeField] private Animator _animator;
    [SerializeField] private float forceMultiply = 2.0f;
    
    private static readonly int Enter = Animator.StringToHash("Enter");
    private PlayerController playerController;

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.CompareTag("Player")) {
            _animator.SetTrigger(Enter);
            playerController = other.gameObject.GetComponent<PlayerController>();
        }
    }

    private void OnTriggerStay2D(Collider2D other) {
        if (other.gameObject.CompareTag("Player")) {
            if (playerController == null) {
                playerController = other.gameObject.GetComponent<PlayerController>();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.CompareTag("Player")) {
            playerController = null;
        }
    }

    public void AddForceJumpPlayer() {
        if (playerController != null) {
            playerController.Force *= forceMultiply;
            playerController.Jump();
            playerController.Force /= forceMultiply;
        }
    }
}
