﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour {

    public int CoinsCount { get; set; }
    public BuffReceiver BuffReceiver;

    public List<Item> Items { get; private set; }

    private void Start() {
        GameManager.Instance.inventory = this;
        Items = new List<Item>();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (GameManager.Instance.coinContainer.ContainsKey(other.gameObject)) {
            var coin = GameManager.Instance.coinContainer[other.gameObject];
            coin.StartDestroy();
        }
        if (GameManager.Instance.itemsContainer.ContainsKey(other.gameObject)) {
            var itemComponent = GameManager.Instance.itemsContainer[other.gameObject];
            Items.Add(itemComponent.Item);
            itemComponent.Destroy(other.gameObject);
        }
    }
}
