﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDamage : MonoBehaviour {

    [SerializeField] private int damage;
    [SerializeField] private bool isDestroyingAfterCollision;
    private IObjectDestroyer destroyer;
    private GameObject parent;
    
    public int Damage {
        get => damage;
        set => damage = value;
    }

    public GameObject Parent {
        get => parent;
        set => parent = value;
    }

    public void Init(IObjectDestroyer destroyer) {
        this.destroyer = destroyer;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject == parent) {
            return;
        }
        if (GameManager.Instance.healthContainer.ContainsKey(other.gameObject)) {
            var health = GameManager.Instance.healthContainer[other.gameObject]; 
            health.TakeHit(damage);
        }
        if (isDestroyingAfterCollision) {
            if (destroyer == null) {
                Destroy(gameObject);
            } else {
                 destroyer.Destroy(gameObject);
            }
        }
    }
}
