﻿using UnityEngine.UI;
using UnityEngine;

public class UICharacterController : MonoBehaviour {

    [SerializeField] private PressButton left;
    [SerializeField] private PressButton right;
    [SerializeField] private Button fire;
    [SerializeField] private Button jump;

    public PressButton Left => left;
    public PressButton Right => right;
    public Button Fire => fire;
    public Button Jump => jump;

    void Start() {
        PlayerController.Instance.InitUIController(this);
    }
    
}
