﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIControl : MonoBehaviour {
    
    private static readonly Color BUTTON_NORMAL_COLOR = new Color(0.1450828f, 0.1094224f, 0.2154269f, 1f);
    private static readonly Color BUTTON_HIGHLIGHED_COLOR = new Color(0.6156863f, 0.2392157f, 0.8392157f, 1f);

    [SerializeField] private GameObject player;
    
    [Header("UI Controls")]
    [SerializeField] private Text coinsCounterView;
    [SerializeField] private Text healthCounterView;
    [SerializeField] private Text endTextView;
    [SerializeField] private Text reloadTime;
    [SerializeField] private GameObject buttonSoundSetting;
    [SerializeField] private GameObject panelMenu;
    [SerializeField] private GameObject inventoryPanel;
    [Header("UI Controls (Health settings)")]
    [SerializeField] private Image healthBar;
    [SerializeField] private float delta;

    private Health _playerHealth;
    private PlayerInventory _playerInventory;
    private PlayerController _playerController;
    private Button _buttonSoundSetting;
    private Text _labelSoundSetting;
    private float _healthValue;
    private float _currentHealth;

    private void Awake() {
        _playerHealth = player.gameObject.GetComponent<Health>();
        _playerInventory = player.gameObject.GetComponent<PlayerInventory>();
        _playerController = player.gameObject.GetComponent<PlayerController>();
        _buttonSoundSetting = buttonSoundSetting.GetComponent<Button>();
        _labelSoundSetting = buttonSoundSetting.GetComponentInChildren<Text>();
    }

    private void Start() {
        _healthValue = _playerHealth.currentHealth / 100.0f;
        GetPlayerData();
        if (!PlayerPrefs.HasKey("Sound")) {
            PlayerPrefs.SetInt("Sound", 1);
        }
        if (PlayerPrefs.GetInt("Sound") == 0) {
            _labelSoundSetting.text = "Звук Выкл.";
            var colorBlock = _buttonSoundSetting.colors;
            colorBlock.normalColor = Color.red;
            colorBlock.highlightedColor = Color.red;
            colorBlock.pressedColor = Color.red;
            colorBlock.selectedColor = Color.red;
            _buttonSoundSetting.colors = colorBlock;
        } else {
            _labelSoundSetting.text = "Звук Вкл.";
            var colorBlock = _buttonSoundSetting.colors;
            colorBlock.normalColor = BUTTON_NORMAL_COLOR;
            colorBlock.highlightedColor = BUTTON_HIGHLIGHED_COLOR;
            colorBlock.pressedColor = BUTTON_NORMAL_COLOR;
            colorBlock.selectedColor = BUTTON_HIGHLIGHED_COLOR;
            _buttonSoundSetting.colors = colorBlock;
        }
    }

    // Update is called once per frame
    private void Update() {
        GetPlayerData();
    }

    private void FixedUpdate() {
        if (Input.GetKey(KeyCode.I)) {
            OnClickInventory();
        }
    }

    private void GetPlayerData() {
        var time = $"{(_playerController != null ? _playerController.ReloadTimer : -1):F2}";
        coinsCounterView.text = _playerInventory != null ? _playerInventory.CoinsCount.ToString() : coinsCounterView.text;
        healthCounterView.text = _playerHealth != null ? _playerHealth.currentHealth + "%" : "0%";
        reloadTime.text = "Reload time: " + time;
        if (_playerController != null && _playerController.IsEnd) {
            endTextView.enabled = true;
        }
        HealthBarAnimation();
    }

    private void HealthBarAnimation() {
        _currentHealth = _playerHealth.currentHealth / 100.0f;
        if (_currentHealth > _healthValue) {
            _healthValue += delta;
        }
        if (_currentHealth < _healthValue) {
            _healthValue -= delta;
        }
        if (_currentHealth < delta) {
            _healthValue = _currentHealth;
        }
        healthBar.fillAmount = _healthValue;
    }

    public void OnClickInventory() {
        if (Time.timeScale > 0) {
            Time.timeScale = 0;
            inventoryPanel.SetActive(true);
        } else {
            inventoryPanel.SetActive(false);
            Time.timeScale = 1;
        }
    }
    
    public void OnClickPause() {
        if (Time.timeScale > 0) {
            Time.timeScale = 0;
            panelMenu.SetActive(true);
        } else {
            panelMenu.SetActive(false);
            Time.timeScale = 1;
        }
    }
    
    public void OnClickExitMenu() {
        panelMenu.SetActive(false);
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void OnClickExit() {
        Application.Quit();
    }

    public void OnClickSoundSettings() {
        if (PlayerPrefs.GetInt("Sound") == 1) {
            _labelSoundSetting.text = "Звук Выкл.";
            var colorBlock = _buttonSoundSetting.colors;
            colorBlock.normalColor = Color.red;
            colorBlock.highlightedColor = Color.red;
            colorBlock.pressedColor = Color.red;
            colorBlock.selectedColor = Color.red;
            _buttonSoundSetting.colors = colorBlock;
            PlayerPrefs.SetInt("Sound", 0);
        } else {
            _labelSoundSetting.text = "Звук Вкл.";
            var colorBlock = _buttonSoundSetting.colors;
            colorBlock.normalColor = BUTTON_NORMAL_COLOR;
            colorBlock.highlightedColor = BUTTON_HIGHLIGHED_COLOR;
            colorBlock.pressedColor = BUTTON_NORMAL_COLOR;
            colorBlock.selectedColor = BUTTON_HIGHLIGHED_COLOR;
            _buttonSoundSetting.colors = colorBlock;
            PlayerPrefs.SetInt("Sound", 1);
        }
    }

}
