﻿using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class UIMenu : MonoBehaviour {

    [SerializeField] private InputField inputField;
    [SerializeField] private GameObject levelChoicer;
    [SerializeField] private GameObject mainMenu;

    private void Start() {
        if (PlayerPrefs.HasKey("Player_Name")) {
            inputField.text = PlayerPrefs.GetString("Player_Name");
        }
    }

    public void OnEndEditName() {
        PlayerPrefs.SetString("Player_Name", inputField.text);
    }

    public void OnClickPlay() {
        SceneManager.LoadScene(1);
    }

    public void OnClickExit() {
        Application.Quit();
    }

    public void OnClickLevelSelect() {
        mainMenu.SetActive(false);
        levelChoicer.SetActive(true);
    }

    public void OnClickBackToMenu() {
        levelChoicer.SetActive(false);
        mainMenu.SetActive(true);
    }

    public void OnClickLevel(int levelId) {
        SceneManager.LoadScene(levelId);
    }

}
